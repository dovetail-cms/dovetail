from datetime import date
import json

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.core.validators import MaxValueValidator
from django.db import models, IntegrityError
from django.forms import CharField, EmailField, Form, HiddenInput, IntegerField, NumberInput, TextInput
from django.template.response import TemplateResponse
from django.utils.crypto import get_random_string

from modelcluster.fields import ParentalKey

from wagtail.admin.edit_handlers import FieldPanel, FieldRowPanel, HelpPanel, InlinePanel, MultiFieldPanel
from wagtail.admin.mail import send_mail
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField, AbstractFormSubmission
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel

from home.models import BootstrapEmailForm


class EventIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full"),
    ]

    parent_page_types = ['home.HomePage', 'ministry.MinistryPage']
    subpage_types = ['event.EventPage']

    def get_context(self, request):
        from home.models import HomePage
        # Update context to include only published events, ordered by recent date
        context = super().get_context(request)
        if self.get_parent().specific_class == HomePage:
            events = EventPage.objects.descendant_of(self.get_parent()).live()  # If this is the main events page, list all events in the tree.
        else:
            events = EventPage.objects.child_of(self).live()  # Otherwise just list this indices children.

        # Filter events list to get ones that are either running now or start in the future
        events = events.filter(date_from__gte=date.today())
        events = events.order_by('date_from')  # Order by date

        context['events'] = events
        return context

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Event Index Page'
        verbose_name_plural = 'Event Index Pages'


class EventPage(Page):
    class EventChoices(models.TextChoices):
        ORGANIZER = 'Organizer', 'Organizer'
        SPEAKER = 'Speaker', 'Speaker'

    date_from = models.DateField("Start date")
    date_to = models.DateField(
        "End date",
        null=True,
        blank=True,
        help_text="Not required if event is on a single day"
    )
    time_from = models.TimeField("Start time", null=True, blank=True)
    time_to = models.TimeField("End time", null=True, blank=True)
    location = models.CharField(max_length=255, help_text="Event Location, e.g. Main Sanctuary, Town Park")
    address = models.CharField(max_length=255, blank=True, help_text="Event Address or Open Location Code")
    intro = RichTextField(features=["bold", "italic"], blank=True, help_text="A brief description of the event for indexes")
    body = RichTextField(blank=True)
    cost = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    is_canceled = models.BooleanField("Mark event canceled", default=False)

    sidebar_type = models.CharField(
        max_length=16,
        choices=EventChoices.choices,
        default=EventChoices.ORGANIZER,
    )
    sidebar_name = models.CharField(max_length=255)
    sidebar_phone = models.CharField(max_length=16, null=True, blank=True)
    sidebar_email = models.EmailField(null=True, blank=True)
    hero_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='A high-res image for the top of the event details page.'
    )
    sidebar_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='An event related image or image of the event organizer for the sidebar.',
    )
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='An small image for the event index page. Can be the same as the hero image.',
    )

    content_panels = Page.content_panels + [
        FieldPanel('date_from'),
        FieldPanel('date_to'),
        FieldPanel('time_from'),
        FieldPanel('time_to'),
        FieldPanel('location'),
        FieldPanel('address'),
        FieldPanel('cost'),
        MultiFieldPanel([
            FieldPanel('sidebar_type'),
            FieldPanel('sidebar_name'),
            FieldPanel('sidebar_phone'),
            FieldPanel('sidebar_email'),
        ], heading='Primary Event Contact or Speaker Info'),
        ImageChooserPanel('hero_image'),
        ImageChooserPanel('sidebar_image'),
        FieldPanel('body', classname="full"),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('is_canceled'),
        FieldPanel('intro'),
        ImageChooserPanel('feed_image'),
    ]

    parent_page_types = ['event.EventIndexPage']
    subpage_types = ['event.EventFormPage']

    def get_context(self, request):
        context = super().get_context(request)
        context['maps_api_key'] = getattr(settings, 'MAPS_API_KEY', "")
        return context

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Event Page'
        verbose_name_plural = 'Event Pages'


def validate_label_not_reserved(label):
    if label.lower() in ['name', 'email', 'attendee_count']:  # A lowercase list of reserved labels for event signups.:
        raise ValidationError(f"'{label}' is a reserved label, please choose another label.")


class NonReservedFormField(AbstractFormField):
    page = ParentalKey('EventFormPage', on_delete=models.CASCADE, related_name='form_fields')

    label = models.CharField(
        verbose_name='label',
        max_length=255,
        help_text=f"The label of the form field, cannot be one of the following:{['name', 'email', 'attendee_count']}",
        validators=[validate_label_not_reserved]
    )


class EventFormPage(BootstrapEmailForm):
    intro = RichTextField(
        blank=True,
        features=['bold', 'italic', 'h2', 'h3', 'h4', 'ol', 'ul', 'hr', 'link', 'document-link', 'image']
    )
    thank_you_text = RichTextField(
        blank=True,
        features=['bold', 'italic', 'h2', 'h3', 'h4', 'ol', 'ul', 'hr', 'link', 'document-link', 'image']
    )
    capacity = models.PositiveSmallIntegerField(default=0, help_text='For no capacity limit, enter 0.')

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('intro', classname="full"),
        FieldPanel('thank_you_text', classname='full'),
        FieldPanel('capacity'),
        HelpPanel(
            heading='Included Form Fields',
            content='Form Fields for Name, Email, and Number of Attendees are already included.',
        ),
        InlinePanel('form_fields', label='Form fields', help_text='Add other form fields as needed.'),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname='col6'),
                FieldPanel('to_address', classname='col6'),
            ]),
            FieldPanel('subject'),
        ], 'Email Notification Config'),
    ]

    @property
    def attendee_count(self):
        submissions = self.get_submission_class().objects.filter(page=self)
        return sum([submission.get_data()['attendee_count'] for submission in submissions])

    @property
    def space_left(self):
        return self.capacity - self.attendee_count

    def get_data_fields(self):
        data_fields = [
            ('name', 'Name'),
            ('email', 'Email'),
            ('attendee_count', 'Number of Attendees'),
        ]
        data_fields += super().get_data_fields()

        return data_fields

    def serve(self, request, *args, **kwargs):
        space_left = self.space_left
        if request.method == 'POST':
            form = EventForm(request.POST)
            if self.capacity:
                form.fields['attendee_count'].validators.append(MaxValueValidator(space_left))
            wagtail_form = self.get_form(request.POST, request.FILES, page=self, user=request.user)

            if form.is_valid() and wagtail_form.is_valid():
                try:
                    form_submission = self.process_form_submission(form, wagtail_form)
                    return self.render_landing_page(request, form_submission, *args, **kwargs)
                except IntegrityError:
                    duplicate_form = self.get_submission_class().objects.filter(
                        page=self,
                        name=form.cleaned_data['name'],
                        email=form.cleaned_data['email'],
                    ).get().get_data()
                    cancellation_id = duplicate_form['cancellation_id']
                    send_mail(
                        self.subject,
                        f'To cancel your reservation follow this link: {self.full_url.rstrip("/")}?cancellation_id={cancellation_id}',
                        (duplicate_form['email'],),
                        self.from_address
                    )
                    form.add_error(
                        None,
                        (
                            'This combination of name and email has already been used to make a reservation. '
                            'The cancellation email to your previous reservation has been resent. '
                            'If you wish to modify your reservation, please cancel and refill the form with updated information.'
                        )
                    )
                    form.add_error('name', 'Combination of Name and Email must be unique.')
                    form.add_error('email', 'Combination of Name and Email must be unique.')
        else:
            cancellation_id = request.GET.get('cancellation_id')
            if cancellation_id:
                cancelled_resultset = self.get_submission_class().objects.filter(page=self, cancellation_id=cancellation_id)
                try:
                    cancelled_form = cancelled_resultset.get()
                except ObjectDoesNotExist:
                    return TemplateResponse(
                        request,
                        '400.html',
                        {'error_message': 'This cancellation does not exist or has already been completed.'}
                    )
                cancelled_resultset.delete()
                return TemplateResponse(
                    request,
                    'event/event_form_cancelled.html',
                    {'cancelled_submission': cancelled_form.get_data()}
                )
            else:
                form = EventForm()
                wagtail_form = self.get_form(page=self, user=request.user)

        if not self.capacity:
            form.fields['attendee_count'].widget = HiddenInput()
        else:
            form.fields['attendee_count'].widget.attrs.update({'max': space_left})

        context = self.get_context(request)
        context['form'] = form
        context['wagtail_form'] = wagtail_form
        return TemplateResponse(
            request,
            self.get_template(request),
            context
        )

    def render_email(self, form, cancellation_id):
        content = [f'Your reservation has been submitted.', '',  # Add a reservation notice.
                   f'To cancel your reservation follow this link: {self.full_url.rstrip("/")}?cancellation_id={cancellation_id}', '',
                   '', super().render_email(form), '',  # Get the original content (string)
                   '{}: {}'.format('Submitted Via', self.full_url),  # Add a link to the form page
                   f'Submitted on: {date.today()}']  # Add the date the form was submitted

        # Content is joined with a new line to separate each text line
        content = '\n'.join(content)

        return content

    def send_mail(self, form, cancellation_id):
        addresses = [x.strip() for x in self.to_address.split(',')]
        addresses.append(form.cleaned_data['email'])
        send_mail(self.subject, self.render_email(form, cancellation_id), addresses, self.from_address)

    def get_submission_class(self):
        return EventFormSubmission

    def process_form_submission(self, form, wagtail_form):
        cancellation_id = get_random_string(7)
        submission = self.get_submission_class().objects.create(
            form_data=json.dumps(wagtail_form.cleaned_data, cls=DjangoJSONEncoder),
            page=self,
            name=form.cleaned_data['name'],
            email=form.cleaned_data['email'],
            attendee_count=form.cleaned_data['attendee_count'],
            cancellation_id=cancellation_id
        )
        self.send_mail(form, cancellation_id)
        return submission

    parent_page_types = ['event.EventPage']
    subpage_types = []

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Event Signup Page'
        verbose_name_plural = 'Event Signup Pages'


class EventFormSubmission(AbstractFormSubmission):
    """
    Cancellable submission with signup info for an event: name, email, number of people.
    """
    name = models.CharField(max_length=100)
    email = models.EmailField()
    attendee_count = models.IntegerField()
    cancellation_id = models.CharField(max_length=7)

    def get_data(self):
        form_data = super().get_data()
        form_data.update({
            'name': self.name,
            'email': self.email,
            'attendee_count': self.attendee_count,
            'cancellation_id': self.cancellation_id,
        })
        return form_data

    class Meta:
        unique_together = ('name', 'email', 'page_id')  # One name/email signup per event (page_id).


class EventForm(Form):
    """
    Common event form: name, email number of people.
    """
    name = CharField(
        label='Name',
        help_text='First and last name',
        widget=TextInput(attrs={'placeholder': 'John Doe'})
    )
    email = EmailField(
        label='Email address',
        help_text='We\'ll never share your email with anyone else',
        widget=TextInput(attrs={'placeholder': 'john.doe@example.com'})
    )
    attendee_count = IntegerField(
        label='Number of attendees',
        help_text='Enter the number of attendees (including yourself).',
        initial=1,
        min_value=1,
    )
