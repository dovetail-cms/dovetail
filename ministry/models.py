from datetime import date

from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.blocks import CharBlock, ListBlock, PageChooserBlock, StructBlock
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from wagtail.embeds.embeds import get_embed
from wagtail.images.edit_handlers import ImageChooserPanel

from home.blocks import BaseStreamBlock
from blog.models import BlogIndexPage, BlogPage
from event.models import EventIndexPage, EventPage
from videoteaching.models import VideoTeachingIndexPage, VideoTeachingPage


class MinistryBlock(StructBlock):
    title = CharBlock(required=True, help_text="Ministry Section Heading")
    ministries = ListBlock(PageChooserBlock(target_model='ministry.MinistryPage'))

    class Meta:
        template = 'blocks/ministry_block.html'
        icon = 'placeholder'
        label = 'Ministry Group'


class MinistryIndexPage(Page):
    intro = RichTextField(blank=True)
    body = StreamField(
        [
            ('Ministries', MinistryBlock()),
        ],
        null=True,
        blank=True,
    )

    hero_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='A high-res image for the top of the ministry index page.'
    )

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full"),
        ImageChooserPanel('hero_image'),
        StreamFieldPanel('body'),
    ]

    parent_page_types = ['home.HomePage']
    subpage_types = ['ministry.MinistryPage']

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Ministry Index Page'
        verbose_name_plural = 'Ministry Index Pages'


class MinistryPage(Page):
    intro = RichTextField(features=["bold", "italic"], blank=True, help_text="A brief description of the ministry for the index page")
    body = StreamField(
        BaseStreamBlock(), verbose_name="Page content block", blank=True
    )
    hero_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='A high-res image for the top of the event details page.'
    )

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full"),
        StreamFieldPanel('body'),
        ImageChooserPanel('hero_image'),
    ]

    parent_page_types = ['ministry.MinistryIndexPage']
    subpage_types = ['event.EventIndexPage', 'blog.BlogIndexPage', 'videoteaching.VideoTeachingIndexPage', 'home.FormPage']

    def get_context(self, request):
        # Update context to include recent videos, upcoming events and recent blog posts
        context = super().get_context(request)
        context['videoteachings'] = VideoTeachingPage.objects.descendant_of(self).live().order_by('-date')[:3]
        context['videoindex'] = VideoTeachingIndexPage.objects.child_of(self).live().first()
        context['blogposts'] = BlogPage.objects.descendant_of(self).live().order_by('-date')[:3]
        context['blogindex'] = BlogIndexPage.objects.child_of(self).live().first()
        context['events'] = EventPage.objects.descendant_of(self).live().filter(date_from__gte=date.today()).order_by('date_from')[:3]
        context['eventindex'] = EventIndexPage.objects.child_of(self).live().first()
        return context

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Ministry Page'
        verbose_name_plural = 'Ministry Pages'
