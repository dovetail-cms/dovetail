# Contributing to Dovetail

Please feel free to email the project maintainer via email if you would like to be involved in this project.

This project is built with Wagtail, which is itself an extension of the Django project.
Development can be roughly broken down into two sections: the back-end code and front-end HTML templates and CSS.

Code contributions require a working knowledge of Python, and the frameworks this code uses: Django and Wagtail.

Front-end work requires designing templates in HTML that interact with the back-end to deliver a consistent UI for the user.
The Bootstrap toolkit is being used to quickly design responsive layouts.

## Issues

The easiest way to contribute to is to work on existing issues, or create and work on new bug reports or feature requests.

