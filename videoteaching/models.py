from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import models

from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey

from taggit.models import TaggedItemBase

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, PageChooserPanel
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel


class VideoTeachingIndexPage(RoutablePageMixin, Page):
    intro = RichTextField()
    featured_video = models.ForeignKey(
        'videoteaching.VideoTeachingPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Set a featured video teaching page. Leave blank to default to the newest video.'
    )

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
        PageChooserPanel('featured_video', 'videoteaching.VideoTeachingPage'),
    ]

    parent_page_types = ['home.HomePage', 'ministry.MinistryPage']
    subpage_types = [
        'videoteaching.VideoTeachingPage',
        'videoteaching.VideoTeachingSeriesPage',
    ]

    def get_series(self):
        series = VideoTeachingSeriesPage.objects.descendant_of(self).live()
        return sorted(
            series,
            key=lambda s: VideoTeachingPage.objects.child_of(s).order_by('-date').first().date,
            reverse=True)

    def get_context(self, request):
        # Update context to include only published posts, ordered by recent date
        context = super().get_context(request)
        videoteachings = VideoTeachingPage.objects.descendant_of(self).live().order_by('-date')
        tags = VideoTeachingPage.tags
        # Filter by tag if given, otherwise prepare default index page
        active_tags = request.GET.getlist('tag')
        if active_tags:
            context['active_tags'] = active_tags
            videoteachings = videoteachings.filter(tags__slug__in=active_tags).distinct()
            tags = tags.filter(videoteachingpage__in=videoteachings).exclude(slug__in=active_tags)
        else:
            tags = tags.filter(videoteachingpage__in=videoteachings)
            context['videoseries'] = self.get_series()[:3]
            if context['page'].featured_video:
                videoteachings = videoteachings.not_page(context['page'].featured_video)
            else:
                context['page'].featured_video = videoteachings.first()
                videoteachings = videoteachings[1:]

        paginator = Paginator(videoteachings, per_page=24, orphans=9)

        page = request.GET.get("page")
        try:
            page_videoteachings = paginator.page(page)
        except PageNotAnInteger:
            page_videoteachings = paginator.page(1)
        except EmptyPage:
            page_videoteachings = paginator.page(paginator.num_pages)

        context['videoteachings'] = page_videoteachings
        context['tags'] = tags.order_by('name')
        return context

    @route(r'^series/$', name='series')
    def videoteaching_series_index(self, request):
        # View a listing of all the series
        series = Paginator(self.get_series(), per_page=24, orphans=2)

        page = request.GET.get("page")
        try:
            series = series.page(page)
        except PageNotAnInteger:
            series = series.page(1)
        except EmptyPage:
            series = series.page(series.num_pages)

        return self.render(
            request,
            context_overrides={
                'videoseries': series,
            },
            template='videoteaching/video_teaching_series_index.html',
        )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Video Teaching Index Page'
        verbose_name_plural = 'Video Teaching Index Pages'


class VideoTeachingPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'VideoTeachingPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )


class VideoTeachingSeriesPage(Page):
    intro = RichTextField(blank=True)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='A small image for the index page. Use a 16x9 aspect ratio.'
    )

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
        ImageChooserPanel('feed_image', )
    ]

    parent_page_types = ['videoteaching.VideoTeachingIndexPage']
    subpage_types = ['videoteaching.VideoTeachingPage']

    def get_context(self, request):
        # Update context to include only published posts, ordered by recent date
        context = super().get_context(request)
        videoteachings = VideoTeachingPage.objects.child_of(self).live().order_by('-date')
        context['videoteachings'] = videoteachings
        return context

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Video Teaching Series Page'
        verbose_name_plural = 'Video Teaching Series Pages'


class VideoTeachingPage(Page):
    author = models.CharField('Author', max_length=255)
    date = models.DateField('Date')
    video = models.URLField()
    intro = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=VideoTeachingPageTag, blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('author'),
            FieldPanel('date'),
            FieldPanel('tags'),
        ], heading="Teaching information"),
        FieldPanel('video', classname='full'),
        FieldPanel('intro', classname='full'),
    ]

    parent_page_types = [
        'videoteaching.VideoTeachingIndexPage',
        'videoteaching.VideoTeachingSeriesPage',
    ]
    subpage_types = []

    def get_similar_videos(self):
        # Get similar videos based on tags.
        index = self.get_ancestors().type(VideoTeachingIndexPage).first()
        similar = VideoTeachingPage.objects.descendant_of(index).live().filter(tags__in=self.tags.all()).distinct()
        # if self.get_parent().
        return similar

    def get_context(self, request):
        context = super().get_context(request)
        # Add related videos
        # context['relatedvideos'] = self.tags.similar_objects()
        related = self.get_similar_videos()  #todo: replace with similar_objects() once the wagtail issue is resolved.
        if self.get_parent().specific_class == VideoTeachingSeriesPage:
            context['seriesvideos'] = VideoTeachingPage.objects.sibling_of(self, inclusive=False).order_by('-date')
            context['relatedvideos'] = related.not_sibling_of(other=self)[:6]
        else:
            context['relatedvideos'] = related[:6]
        return context

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Video Teaching Page'
        verbose_name_plural = 'Video Teaching Pages'
