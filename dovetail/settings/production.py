from .base import *

DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['DJANGO_PRODUCTION_SECRET_KEY']

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

CSRF_COOKIE_SECURE = True

SESSION_COOKIE_SECURE = True

# Google Maps API key
# SECURITY WARNING: keep the API key used in production secret!
MAPS_API_KEY = 'YOUR_GOOGLE_MAPS_API_KEY'

try:
    from .local import *
except ImportError:
    pass
