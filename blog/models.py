from django.db import models

from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey

from taggit.models import TaggedItemBase

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, StreamFieldPanel
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField

from home.blocks import BaseStreamBlock


class BlogIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
    ]

    parent_page_types = ['home.HomePage', 'ministry.MinistryPage']
    subpage_types = ['blog.BlogPage']

    def get_context(self, request):
        # Update context to include only published posts, ordered by recent date
        context = super().get_context(request)
        blogpages = BlogPage.objects.descendant_of(self).live().order_by('-date')

        tags = BlogPage.tags
        # Filter by tag if given, otherwise prepare default index page
        active_tags = request.GET.getlist('tag')
        if active_tags:
            context['active_tags'] = active_tags
            blogpages = blogpages.filter(tags__slug__in=active_tags).distinct()
            tags = tags.filter(blogpage__in=blogpages).exclude(slug__in=active_tags)
        else:
            tags = tags.filter(blogpage__in=blogpages)

        context['blogpages'] = blogpages
        context['tags'] = tags.order_by('name')
        return context

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Blog Index Page'
        verbose_name_plural = 'Blog Index Pages'


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'BlogPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )


class BlogPage(Page):
    author = models.CharField('Author', max_length=100)
    date = models.DateField('Post Date')
    intro = RichTextField(blank=True)
    # Body section of the BlogPage
    body = StreamField(
        BaseStreamBlock(), verbose_name='Home content block', blank=True
    )
    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('author'),
            FieldPanel('date'),
            FieldPanel('tags'),
        ], heading="Blog information"),
        FieldPanel('intro', classname='full'),
        StreamFieldPanel('body', classname='full'),
    ]

    parent_page_types = ['blog.BlogIndexPage']
    subpage_types = []

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Blog Page'
        verbose_name_plural = 'Blog Pages'
