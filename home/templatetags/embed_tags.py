from django import template

from wagtail.embeds import embeds
from wagtail.embeds.exceptions import EmbedException


register = template.Library()


@register.simple_tag(name='embed_thumbnail')
def embed_thumbnail_tag(url, max_width=None):
    try:
        embed = embeds.get_embed(url, max_width=max_width)
        return embed.thumbnail_url
    except EmbedException:
        return ''
