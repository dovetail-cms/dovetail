from django import template


register = template.Library()


def is_active(page, current_page):
    # To give us active state on main navigation
    return current_page.url_path.startswith(page.url_path) if current_page else False


# Retrieves the top menu items - the immediate children of the parent page
@register.inclusion_tag('tags/top_menu.html', takes_context=True)
def top_menu(context, parent, calling_page=None):
    menuitems = parent.get_children().live().in_menu()
    for menuitem in menuitems:
        menuitem.show_dropdown = menuitem.get_children().live().in_menu().exists()
        menuitem.active = (calling_page.url_path.startswith(menuitem.url_path) if calling_page else False)
    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        'request': context['request'],  # required by the pageurl tag that we want to use within this template
    }


# Retrieves the children of the top menu items for the drop downs
@register.inclusion_tag('tags/top_menu_children.html', takes_context=True)
def top_menu_children(context, parent, calling_page=None):
    menuitems_children = parent.get_children().live().in_menu()
    return {
        'parent': parent,
        'menuitems_children': menuitems_children,  # required by the pageurl tag that we want to use within this template
        'request': context['request'],
    }


# Used to remove the page= field from a url query string.
@register.simple_tag(takes_context=True)
def url_params_no_page(context):
    params = context['request'].GET.copy()
    if 'page' in params:
        del params['page']
    param_string = params.urlencode()
    if param_string:
        return '&' + param_string
    else:
        return ''
