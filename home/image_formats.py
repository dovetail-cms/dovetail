from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _

from wagtail.core.rich_text import features
from wagtail.images.formats import Format, register_image_format, unregister_image_format


class PictureFormat(Format):
    def __init__(self, name, label, classnames, filter_spec, figure_classnames=None):
        self.figure_classnames = figure_classnames
        super().__init__(name, label, classnames, filter_spec)

    def image_to_html(self, image, alt_text, extra_attributes=None):
        figure_classnames = ' ' + self.figure_classnames if self.figure_classnames else ''
        default_html = super().image_to_html(image, alt_text, extra_attributes)
        return format_html('<figure class="figure{}">{}</figure>',
                           figure_classnames, default_html)


class CaptionedPictureFormat(PictureFormat):
    def image_to_html(self, image, alt_text, extra_attributes=None):
        figure_classnames = ' ' + self.figure_classnames if self.figure_classnames else ''
        default_html = super(PictureFormat, self).image_to_html(image, alt_text, extra_attributes)
        return format_html('<figure class="figure{}">{}<figcaption class="figure-caption">{}</figcaption></figure>',
                           figure_classnames, default_html, alt_text)


register_image_format(CaptionedPictureFormat(
    'captioned_fullwidth', 'Full width captioned', 'figure-img img-fluid rounded', 'width-800'))
features.default_features.append('captioned_fullwidth')

register_image_format(CaptionedPictureFormat(
    'captioned_left', 'Left-aligned captioned', 'figure-img img-fluid rounded', 'width-500', 'float-sm-left text-left col-sm-6 px-0 pr-sm-2'))
features.default_features.append('captioned_left')

register_image_format(CaptionedPictureFormat(
    'captioned_right', 'Right-aligned captioned', 'figure-img img-fluid rounded', 'width-500', 'float-sm-right text-right col-sm-6 px-0 pl-sm-2'))
features.default_features.append('captioned_right')

# Re-define default image formats
unregister_image_format('fullwidth')
unregister_image_format('left')
unregister_image_format('right')

register_image_format(PictureFormat(
    'fullwidth', _('Full width'), 'figure-img img-fluid rounded', 'width-800'))
register_image_format(PictureFormat(
    'left', _('Left-aligned'), 'figure-img img-fluid rounded', 'width-800', 'float-sm-left col-sm-6 px-0 pr-sm-2'))
register_image_format(PictureFormat(
    'right', _('Right-aligned'), 'figure-img img-fluid rounded', 'width-800', 'float-sm-right col-sm-6 px-0 pl-sm-2'))
