from wagtail.core.blocks import (
    CharBlock, ChoiceBlock, EmailBlock, PageChooserBlock, RichTextBlock, StreamBlock, StructBlock, URLBlock,
)
from wagtail.embeds.blocks import EmbedBlock


# Blocks
class HeadingBlock(StructBlock):
    """
    Custom `StructBlock` that allows the user to select bootstrap display-4 or h1 sized headers
    """
    heading_text = CharBlock(classname="title", required=True)
    size = ChoiceBlock(choices=[
        ('', 'Select a header size'),
        ('h1', 'H1'),
        ('display-4', 'Display-4')
    ], blank=True, required=False)

    class Meta:
        icon = "title"
        template = "blocks/heading_block.html"


class CTABlock(StructBlock):
    """
    Custom `StructBlock` that allows the user to create a call to action text and button.
    """

    title = CharBlock(required=True, max_length=64)
    text = RichTextBlock(required=True, features=["bold", "italic"])
    button_page = PageChooserBlock(required=False)
    button_url = URLBlock(required=False)
    button_mailto = EmailBlock(required=False)
    button_text = CharBlock(required=True, default='Learn More', max_length=40)

    color = ChoiceBlock(choices=[
        ('primary', 'primary'),
        ('secondary', 'secondary'),
        ('success', 'success'),
        ('danger', 'danger'),
        ('warning', 'warning'),
        ('info', 'info'),
        ('light', 'light'),
        ('dark', 'dark'),
        ('muted', 'muted'),
        ('white', 'white'),
    ], default='primary', blank=False, required=True)

    class Meta:
        template = "blocks/cta_block.html"
        icon = "pick"
        label = "Call to Action"


# Streams
class BaseStreamBlock(StreamBlock):
    """
    Define the blocks that `StreamField` will utilize
    """
    heading_block = HeadingBlock()
    paragraph_block = RichTextBlock(
        icon="pilcrow",
        template="blocks/paragraph_block.html"
    )
    embed_block = EmbedBlock(
        help_text='Insert an embed URL e.g https://youtu.be/w-I6XTVZXww',
        template="blocks/embed_block.html")
    cta_block = CTABlock()
