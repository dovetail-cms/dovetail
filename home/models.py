from datetime import date

from django import template
from django.db import models
from django.forms import widgets

from modelcluster.fields import ParentalKey

from wagtail.admin.edit_handlers import FieldPanel, FieldRowPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel, PageChooserPanel
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.contrib.settings.models import BaseSetting
from wagtail.contrib.settings.registry import register_setting
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel

from .blocks import BaseStreamBlock


register = template.Library()


class HomePage(Page):
    # Primary CTA
    primary_cta_heading = models.CharField(max_length=100)
    primary_cta_intro = RichTextField(features=['bold', 'italic'], blank=True)
    primary_cta_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    primary_cta_link = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Set a featured page. Leave blank for no CTA link.'
    )
    primary_cta_text = models.CharField(max_length=32, blank=True)

    # Secondary CTA
    secondary_cta_heading = models.CharField(max_length=100)
    secondary_cta_intro = RichTextField(features=['bold', 'italic'], blank=True)
    secondary_cta_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    secondary_cta_link = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Set a featured page. Leave blank for no CTA link.'
    )
    secondary_cta_text = models.CharField(max_length=32, blank=True)

    # Tertiary CTA
    tertiary_cta_heading = models.CharField(max_length=100)
    tertiary_cta_intro = RichTextField(features=['bold', 'italic'], blank=True)
    tertiary_cta_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    tertiary_cta_link = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Set a featured page. Leave blank for no CTA link.'
    )
    tertiary_cta_text = models.CharField(max_length=32, blank=True)

    # Body section of the HomePage
    body = StreamField(
        BaseStreamBlock(required=False), verbose_name="Home content block", blank=True
    )

    primary_video_teaching_index = models.ForeignKey(
        'videoteaching.VideoTeachingIndexPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Set a featured video teaching index to list the 3 most recent posts. Leave blank for no video top 3.'
    )

    primary_blog_index = models.ForeignKey(
        'blog.BlogIndexPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Set a featured blog index to list the 3 most recent posts. Leave blank for no blog top 3.'
    )

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('primary_cta_heading'),
            FieldPanel('primary_cta_intro'),
            ImageChooserPanel('primary_cta_image'),
            PageChooserPanel('primary_cta_link'),
            FieldPanel('primary_cta_text'),
        ], heading='Primary CTA'),
        MultiFieldPanel([
            FieldPanel('secondary_cta_heading'),
            FieldPanel('secondary_cta_intro'),
            ImageChooserPanel('secondary_cta_image'),
            PageChooserPanel('secondary_cta_link'),
            FieldPanel('secondary_cta_text'),
        ], heading='Secondary CTA'),
        MultiFieldPanel([
            FieldPanel('tertiary_cta_heading'),
            FieldPanel('tertiary_cta_intro'),
            ImageChooserPanel('tertiary_cta_image'),
            PageChooserPanel('tertiary_cta_link'),
            FieldPanel('tertiary_cta_text'),
        ], heading='Tertiary CTA'),
        MultiFieldPanel([
            PageChooserPanel('primary_video_teaching_index'),
            PageChooserPanel('primary_blog_index'),
        ], heading='Top 3s'),
        StreamFieldPanel('body'),
    ]

    def get_context(self, request):
        from blog.models import BlogIndexPage, BlogPage
        from event.models import EventIndexPage, EventPage
        from videoteaching.models import VideoTeachingIndexPage, VideoTeachingPage
        # Update context to include recent videos, upcoming events and recent blog posts
        context = super().get_context(request)
        context['videoteachings'] = VideoTeachingPage.objects.descendant_of(self.primary_video_teaching_index).live().order_by('-date')[:3]
        context['blogposts'] = BlogPage.objects.descendant_of(self.primary_blog_index).live().order_by('-date')[:3]
        context['events'] = EventPage.objects.descendant_of(self).live().filter(date_from__gte=date.today()).order_by('date_from')[:3]
        context['eventindices'] = EventIndexPage.objects.child_of(self).live()
        return context

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Home Page"
        verbose_name_plural = "Home Pages"


class FlexPage(Page):
    # Body section of the HomePage
    body = StreamField(
        BaseStreamBlock(), verbose_name="Page content block", blank=True
    )

    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Flexible Page"
        verbose_name_plural = "Flexible Pages"


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', on_delete=models.CASCADE, related_name='form_fields')


class BootstrapEmailForm(AbstractEmailForm):
    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        for field in form:
            # Bootstrap Customization
            if isinstance(field.field.widget, (widgets.CheckboxSelectMultiple, widgets.RadioSelect)):
                field.field.widget.template_name = 'forms/radio.html'
                field.field.widget.option_template_name = 'forms/radio_option.html'
            elif isinstance(field.field.widget, widgets.DateInput):
                field.field.widget.input_type = 'date'
            elif isinstance(field.field.widget, widgets.DateTimeInput):
                field.field.widget.input_type = 'datetime-local'
        return form

    class Meta:
        abstract = True


class FormPage(BootstrapEmailForm):
    intro = RichTextField(
        blank=True,
        features=['bold', 'italic', 'h2', 'h3', 'h4', 'ol', 'ul', 'hr', 'link', 'document-link', 'image']
    )
    thank_you_text = RichTextField(
        blank=True,
        features=['bold', 'italic', 'h2', 'h3', 'h4', 'ol', 'ul', 'hr', 'link', 'document-link', 'image']
    )

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('intro', classname="full"),
        InlinePanel('form_fields', label='Form fields'),
        FieldPanel('thank_you_text', classname='full'),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname='col6'),
                FieldPanel('to_address', classname='col6'),
            ]),
            FieldPanel('subject'),
        ], 'Email Notification Config'),
    ]


@register_setting
class SocialMediaSettings(BaseSetting):
    """Social media settings for our custom website."""

    facebook = models.URLField(blank=True, null=True, help_text="Facebook URL")
    twitter = models.URLField(blank=True, null=True, help_text="Twitter URL")
    youtube = models.URLField(blank=True, null=True, help_text="YouTube Channel URL")

    panels = [
        MultiFieldPanel([
            FieldPanel("facebook"),
            FieldPanel("twitter"),
            FieldPanel("youtube"),
        ], heading="Social Media Settings")
    ]


@register_setting
class FooterSettings(BaseSetting):
    """
    Site footer accessible via the settings.
    """
    slogan = models.TextField()

    address = models.TextField()
    phone = models.CharField(max_length=16)
    email = models.EmailField(max_length=64)
    hours = models.TextField()

    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    panels = [
        FieldPanel('slogan'),
        MultiFieldPanel(
            [
                FieldPanel('address', heading='Church address'),
                FieldPanel('phone', heading="Office phone number"),
                FieldPanel('email', heading='Office email'),
            ], heading='Church contact information'
        ),
        FieldPanel('hours', heading='Office hours'),
        ImageChooserPanel('image', heading='Footer background image'),
    ]


@register_setting
class LivestreamSettings(BaseSetting):
    """
    For adding a livestream link to the Navbar.
    """
    url = models.URLField()
    livestream_from = models.DateTimeField('Start', null=True, blank=True)
    livestream_to = models.DateTimeField('End', null=True, blank=True)

    panels = [
        FieldPanel('url'),
        MultiFieldPanel(
            [
                FieldPanel('livestream_from', heading='Appear'),
                FieldPanel('livestream_to', heading="Disappear"),
            ], heading='When to have the livestream link in the Navbar'
        ),
    ]
