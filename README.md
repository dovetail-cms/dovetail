# Dovetail

*A content-first CMS for churches built with Wagtail.*

---

Create an attractive, customizable website for your church. Spend less time consumed in the layout and design and free your church staff to quickly focus on content. 

This project is still in its early development stage. If you would like to contribute, please refer to the [contribution guide](CONTRIBUTING.md).
If you would like to run this project for your church or ministry, please contact me as there is no **getting started** guide yet.

## Features

* A straight-forward interface for content addition.
* Pages for ministries, sermons, events, blog posts.
* Flexible pages for unspecified content.
* A fast, responsive layout based on bootstrap.
* Easy and intuitive layout for users to navigate.